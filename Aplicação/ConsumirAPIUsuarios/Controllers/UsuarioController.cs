﻿using ConsumirAPIUsuarios.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace ConsumirAPIUsuarios.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario
        public ActionResult Index()
        {
            IEnumerable<usuarioViewModel> usuarios = null;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44390/api");

                //HTTP GET
                var responseTask = client.GetAsync("usuario");
                responseTask.Wait();
                var result = responseTask.Result;

                if(result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsAsync<IList<usuarioViewModel>>();
                    readTask.Wait();

                    usuarios = readTask.Result;
                }
                else
                {
                    usuarios = Enumerable.Empty<usuarioViewModel>();
                    ModelState.AddModelError(string.Empty, "Erro no servidor. Contate o Administrador.");
                }
                return View(usuarios);
            }
        }
    }
}