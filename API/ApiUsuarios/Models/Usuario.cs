﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ApiUsuarios.Models
{
    [Table("Usuarios")]
    public class Usuario
    {
        [Key]
        public int Id { get; set; }

        [StringLength(100)]
        public string Nome { get; set; }
        [StringLength(13)]
        public string CPF { get; set; }
        [StringLength(150)]
        public string Email { get; set; }
        [StringLength(40)]
        public string Telefone { get; set; }
        [StringLength(1)]
        public string Sexo { get; set; }
        public DateTime DataNascimento { get; set; }

    }
}