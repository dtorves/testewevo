﻿using System.Data.Entity;

namespace ApiUsuarios.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext() : base("bancoDados")
        {

        }

        public DbSet<Usuario> Usuarios { get; set; }
    }
}