﻿using ApiUsuarios.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiUsuarios.Controllers
{
    public class UsuariosController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetTodosUsuarios()
        {
            IList<Usuario> usuarios = null;

            using (var ctx = new AppDbContext())
            {
                usuarios = ctx.Usuarios.ToList()
                    .Select(s => new Usuario()
                    {
                        Id = s.Id,
                        Nome = s.Nome,
                        CPF = s.CPF,
                        Email = s.Email,
                        Telefone = s.Telefone,
                        Sexo = s.Sexo,
                        DataNascimento = s.DataNascimento   
                    }).ToList();
            }

            if(usuarios.Count == 0)
            {
                return NotFound();
            }

            return Ok(usuarios);
        }

        public IHttpActionResult GetUsuariosPorId(int? id)
        {
            if (id == null)
                return BadRequest("O Id do usuário é inválido");

            Usuario usuario = null;

            using(var ctx = new AppDbContext())
            {
                usuario = ctx.Usuarios.ToList()
                    .Where(c => c.Id == id)
                    .Select(c => new Usuario()
                    {
                        Id = c.Id,
                        Nome = c.Nome,
                        CPF = c.CPF,
                        Email = c.Email,
                        Telefone = c.Telefone,
                        Sexo = c.Sexo,
                        DataNascimento = c.DataNascimento

                    }).FirstOrDefault<Usuario>();
            }

            if(usuario == null)
            {
                return NotFound();
            }

            return Ok(usuario);
        }

        public IHttpActionResult GetUsuarioPorNome(string nome)
        {
            {
                if (nome == null)
                    return BadRequest("Nome Inválido");

                IList<Usuario> usuario = null;
                using (var ctx = new AppDbContext())
                {
                    usuario = ctx.Usuarios.ToList()
                        .Where(s => s.Nome.ToLower() == nome.ToLower())
                        .Select(s => new Usuario()
                        {
                            Id = s.Id,
                            Nome = s.Nome,
                            CPF = s.CPF,
                            Email = s.Email,
                            Telefone = s.Telefone,
                            Sexo = s.Sexo,
                            DataNascimento = s.DataNascimento
                        }).ToList();
                }
                if (usuario.Count == 0)
                {
                    return NotFound();
                }
                return Ok(usuario);
            }
        }

        public IHttpActionResult PostNovoUsuario(UsuarioDTO usuario)
        {
            if (!ModelState.IsValid || usuario == null)
                return BadRequest("Dados do usuário inválidos.");
            using (var ctx = new AppDbContext())
            {
                ctx.Usuarios.Add(new Usuario()
                {
                    Nome = usuario.Nome,
                    CPF = usuario.CPF,
                    Email = usuario.Email,
                    Telefone = usuario.Telefone,
                    Sexo = usuario.Sexo,
                    DataNascimento = usuario.DataNascimento
                });
                ctx.SaveChanges();
            }
            return Ok(usuario);
        }

        public IHttpActionResult PutUsuario(Usuario usuario)
        {
            if (!ModelState.IsValid || usuario == null)
                return BadRequest("Dados do usuário inválidos");
            using (var ctx = new AppDbContext())
            {
                var usuarioSelecionado = ctx.Usuarios.Where(c => c.Id == usuario.Id)
                                                           .FirstOrDefault<Usuario>();
                if (usuarioSelecionado != null)
                {
                    usuarioSelecionado.Nome = usuario.Nome;
                    usuarioSelecionado.CPF = usuario.CPF;
                    usuarioSelecionado.Email = usuario.Email;
                    usuarioSelecionado.Telefone = usuario.Telefone;
                    usuarioSelecionado.DataNascimento = usuario.DataNascimento;
                    ctx.Entry(usuarioSelecionado).State = EntityState.Modified;
         
                    ctx.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }
            return Ok($"Usuário {usuario.Nome} atualizado com sucesso");
        }

        public IHttpActionResult DeleteUsuario(int? id)
        {
            if (id == null)
                return BadRequest("Dados inválidos");
            using (var ctx = new AppDbContext())
            {
                var usuarioSelecionado = ctx.Usuarios.Where(c => c.Id == id)
                                                           .FirstOrDefault<Usuario>();
                if (usuarioSelecionado != null)
                {
                    ctx.Entry(usuarioSelecionado).State = EntityState.Deleted;
                    
                    ctx.SaveChanges();
                }
                else
                {
                    return NotFound();
                }
            }
            return Ok($"Usuário {id} foi deletado com sucesso");
        }
    }
}
